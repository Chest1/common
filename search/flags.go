package search

import (
	"github.com/urfave/cli"
	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/walk"
)

const (
	flagIgnoredDirs      = "ignored-dirs"
	flagIgnoreHiddenDirs = "ignore-hidden-dirs"
	flagMaxDepth         = "max-depth"

	envVarIgnoredDirs      = "SEARCH_IGNORED_DIRS"
	envVarIgnoreHiddenDirs = "SEARCH_IGNORE_HIDDEN_DIRS"
	envVarMaxDepth         = "SEARCH_MAX_DEPTH"
)

var defaultIgnoredDirs = cli.StringSlice([]string{"bundle", "node_modules", "vendor", "tmp", "test", "tests"})

// NewFlags generates command line flags to configure the search.
func NewFlags() []cli.Flag {
	return []cli.Flag{
		cli.StringSliceFlag{
			Name:   flagIgnoredDirs,
			EnvVar: envVarIgnoredDirs,
			Usage:  "Directory to be ignored",
			Value:  &defaultIgnoredDirs,
		},
		cli.BoolTFlag{
			Name:   flagIgnoreHiddenDirs,
			EnvVar: envVarIgnoreHiddenDirs,
			Usage:  "Ignore hidden directories",
		},
		cli.IntFlag{
			Name:   flagMaxDepth,
			EnvVar: envVarMaxDepth,
			Usage:  "Maximum directory depth",
			Value:  2,
		},
	}
}

// NewOptions search options from command line arguments
func NewOptions(c *cli.Context) *walk.Options {
	return &walk.Options{
		MaxDepth:         c.Int(flagMaxDepth),
		IgnoredDirs:      c.StringSlice(flagIgnoredDirs),
		IgnoreHiddenDirs: c.BoolT(flagIgnoreHiddenDirs),
	}
}
