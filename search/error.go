package search

// ErrNotFound is emitted when the project dir tree search
// is finished but no matching files are found.
type ErrNotFound struct {
	searchPath string
}

func (e ErrNotFound) Error() string {
	return "No match in " + e.searchPath
}

// ExitCode returns the analyzer CLI application
// exit code which should be returned upon analyzer
// termination when ErrNotFound occurs.
func (e ErrNotFound) ExitCode() int {
	return 3
}
