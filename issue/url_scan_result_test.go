package issue

import (
	"bytes"
	"encoding/json"
	"reflect"
	"testing"
)

var testURLSuccessResult = URLScanResult{
	URL:                "http://nginx",
	Processed:          true,
	ReasonNotProcessed: "",
	HTTPVerb:           "GET",
	HTTPStatusCode:     200,
	HTTPStatusReason:   "OK",
}

const testURLSuccessJSON = `{
  "url": "http://nginx",
  "processed": true,
  "http_verb": "GET",
  "http_status_code": 200,
  "http_status_reason": "OK"
}`

var testURLFailResult = URLScanResult{
	URL:                "http://nginx/sitemap.xml",
	Processed:          true,
	ReasonNotProcessed: "",
	HTTPVerb:           "GET",
	HTTPStatusCode:     404,
	HTTPStatusReason:   "Not Found",
}

const testURLFailJSON = `{
  "url": "http://nginx/sitemap.xml",
  "processed": true,
  "http_verb": "GET",
  "http_status_code": 404,
  "http_status_reason": "Not Found"
}`

var testURLNotProcessedResult = URLScanResult{
	URL:                "http://nginx/robots.txt",
	Processed:          false,
	ReasonNotProcessed: "Max Children", // message from ZAProxy filter
	HTTPVerb:           "GET",
	HTTPStatusCode:     200,
	HTTPStatusReason:   "OK",
}

const testURLNotProcessedJSON = `{
  "url": "http://nginx/robots.txt",
  "processed": false,
  "reason_not_processed": "Max Children",
  "http_verb": "GET",
  "http_status_code": 200,
  "http_status_reason": "OK"
}`

func TestURLScanResult(t *testing.T) {
	t.Run("MarshalJSON", func(t *testing.T) {
		var tcs = []struct {
			Name   string
			Result URLScanResult
			JSON   string
		}{
			{
				Name:   "Success",
				Result: testURLSuccessResult,
				JSON:   testURLSuccessJSON,
			},
			{
				Name:   "Failure",
				Result: testURLFailResult,
				JSON:   testURLFailJSON,
			},
			{
				Name:   "Not Processed",
				Result: testURLNotProcessedResult,
				JSON:   testURLNotProcessedJSON,
			},
		}

		for _, tc := range tcs {
			t.Run(tc.Name, func(t *testing.T) {
				b, err := json.Marshal(tc.Result)
				if err != nil {
					t.Fatal(err)
				}

				var buf bytes.Buffer
				err = json.Indent(&buf, b, "", "  ")
				if err != nil {
					t.Fatal(err)
				}

				got := buf.String()
				if got != tc.JSON {
					t.Errorf("Wrong result. Expected:\n%#v\nBut got:\n%#v", tc.JSON, got)
				}
			})
		}
	})

	t.Run("UnmarshalJSON", func(t *testing.T) {
		var tcs = []struct {
			Name   string
			JSON   string
			Result URLScanResult
		}{
			{
				Name:   "Success",
				JSON:   testURLSuccessJSON,
				Result: testURLSuccessResult,
			},
			{
				Name:   "Failure",
				JSON:   testURLFailJSON,
				Result: testURLFailResult,
			},
			{
				Name: "Not Processed",

				JSON:   testURLNotProcessedJSON,
				Result: testURLNotProcessedResult,
			},
		}

		for _, tc := range tcs {
			t.Run(tc.Name, func(t *testing.T) {
				var got URLScanResult
				if err := json.Unmarshal([]byte(tc.JSON), &got); err != nil {
					t.Fatal(err)
				}

				if !reflect.DeepEqual(got, tc.Result) {
					t.Errorf("Wrong result. Expected:\n%#v\nBut got:\n%#v", tc.Result, got)
				}
			})
		}
	})
}
