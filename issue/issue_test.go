package issue

import (
	"bytes"
	"encoding/json"
	"reflect"
	"testing"
)

var testSASTIssue = Issue{
	Category:    CategorySast,
	Name:        "Possible command injection",
	Message:     "Possible command injection in application_controller",
	Description: "The ciphertext produced is susceptible to alteration by an adversary. This mean that the cipher provides no wayto detect that the data has been tampered with. If the ciphertext can be controlled by an attacker, it couldbe altered without detection.\n\nThe solution is to used a cipher that includes a Hash basedMessage Authentication Code (HMAC) to sign the data. Combining a HMAC function to the existing cipher is proneto error [1] ( http://www.thoughtcrime.org/blog/the-cryptographic-doom-principle/ ).Specifically, it is always recommended that you be able to verify the HMAC first, and only if the data is unmodified,do you then perform any cryptographic functions on the data.\n\nThe following modes are vulnerablebecause they don't provide a HMAC:",
	CompareKey:  "09abf078636daa3bc6a3d49fba232774a5d9b7c454e1003e6ab12cd56938296a",
	Location: Location{
		File:      "app/controllers/application_controller.rb",
		LineStart: 831,
		LineEnd:   832,
	},
	Scanner: Scanner{
		ID:   "brakeman",
		Name: "Brakeman",
	},
	Severity:   SeverityLevelMedium,
	Confidence: ConfidenceLevelHigh,
	Solution:   "Use the system(command, parameters) method which passes command line parameters safely.",
	Identifiers: []Identifier{
		CVEIdentifier("CVE-2018-1234"),
	},
	Links: []Link{
		{
			Name: "Awesome-security blog post",
			URL:  "https://example.com/blog-post",
		},
		{
			URL: "https://example.com/another-blog-post",
		},
	},
}

const testSASTIssueJSON = `{
  "category": "sast",
  "name": "Possible command injection",
  "message": "Possible command injection in application_controller",
  "description": "The ciphertext produced is susceptible to alteration by an adversary. This mean that the cipher provides no wayto detect that the data has been tampered with. If the ciphertext can be controlled by an attacker, it couldbe altered without detection.\n\nThe solution is to used a cipher that includes a Hash basedMessage Authentication Code (HMAC) to sign the data. Combining a HMAC function to the existing cipher is proneto error [1] ( http://www.thoughtcrime.org/blog/the-cryptographic-doom-principle/ ).Specifically, it is always recommended that you be able to verify the HMAC first, and only if the data is unmodified,do you then perform any cryptographic functions on the data.\n\nThe following modes are vulnerablebecause they don't provide a HMAC:",
  "cve": "09abf078636daa3bc6a3d49fba232774a5d9b7c454e1003e6ab12cd56938296a",
  "severity": "Medium",
  "confidence": "High",
  "solution": "Use the system(command, parameters) method which passes command line parameters safely.",
  "scanner": {
    "id": "brakeman",
    "name": "Brakeman"
  },
  "location": {
    "file": "app/controllers/application_controller.rb",
    "start_line": 831,
    "end_line": 832,
    "dependency": {
      "package": {}
    }
  },
  "identifiers": [
    {
      "type": "cve",
      "name": "CVE-2018-1234",
      "value": "CVE-2018-1234",
      "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-1234"
    }
  ],
  "links": [
    {
      "name": "Awesome-security blog post",
      "url": "https://example.com/blog-post"
    },
    {
      "url": "https://example.com/another-blog-post"
    }
  ]
}`

var testDependencyScanningVulnerability = DependencyScanningVulnerability{Issue{
	Category: CategoryDependencyScanning,
	Scanner: Scanner{
		ID:   "gemnasium",
		Name: "Gemnasium",
	},
	Location: Location{
		File: "app/pom.xml",
		Dependency: Dependency{
			Package: Package{
				Name: "io.netty/netty",
			},
			Version: "3.9.1.Final",
		},
	},
	Identifiers: []Identifier{
		CVEIdentifier("CVE-2018-1234"),
	},
}}

const testDependencyScanningVulnerabilityJSON = `{
  "category": "dependency_scanning",
  "message": "Vulnerability in io.netty/netty",
  "cve": "app/pom.xml:io.netty/netty:cve:CVE-2018-1234",
  "scanner": {
    "id": "gemnasium",
    "name": "Gemnasium"
  },
  "location": {
    "file": "app/pom.xml",
    "dependency": {
      "package": {
        "name": "io.netty/netty"
      },
      "version": "3.9.1.Final"
    }
  },
  "identifiers": [
    {
      "type": "cve",
      "name": "CVE-2018-1234",
      "value": "CVE-2018-1234",
      "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-1234"
    }
  ]
}`

const testDependencyScanningIssueJSON = `{
  "category": "dependency_scanning",
  "scanner": {
    "id": "gemnasium",
    "name": "Gemnasium"
  },
  "location": {
    "file": "app/pom.xml",
    "dependency": {
      "package": {
        "name": "io.netty/netty"
      },
      "version": "3.9.1.Final"
    }
  },
  "identifiers": [
    {
      "type": "cve",
      "name": "CVE-2018-1234",
      "value": "CVE-2018-1234",
      "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-1234"
    }
  ]
}`

var testDASTIssue = Issue{
	Category:    CategoryDast,
	Name:        "Absence of Anti-CSRF Tokens",
	Message:     "No known Anti-CSRF token [anticsrf, CSRFToken, __RequestVerificationToken, csrfmiddlewaretoken, authenticity_token, OWASP_CSRFTOKEN, anoncsrf, csrf_token, _csrf, _csrfSecret] was found in the following HTML form: [Form 1: \"exampleInputEmail1\" \"exampleInputPassword1\" ].",
	Description: "No Anti-CSRF tokens were found in a HTML submission form\nA cross-site request forgery is an attack that involves forcing a victim to send an HTTP request to a target destination without their knowledge or intent in order to perform an action as the victim. The underlying cause is application functionality using predictable URL/form actions in a repeatable way. The nature of the attack is that CSRF exploits the trust that a web site has for a user. By contrast, cross-site scripting (XSS) exploits the trust that a user has for a web site. Like XSS, CSRF attacks are not necessarily cross-site, but they can be. Cross-site request forgery is also known as CSRF, XSRF, one-click attack, session riding, confused deputy, and sea surf.\n\nCSRF attacks are effective in a number of situations, including:\n    * The victim has an active session on the target site.\n    * The victim is authenticated via HTTP auth on the target site.\n    * The victim is on the same local network as the target site.\n\nCSRF has primarily been used to perform an action against a target site using the victim's privileges, but recent techniques have been discovered to disclose information by gaining access to the response. The risk of information disclosure is dramatically increased when the target site is vulnerable to XSS, because XSS can be used as a platform for CSRF, allowing the attack to operate within the bounds of the same-origin policy.\n",
	CompareKey:  "32c8fea43be873a2f1186b801603ead805ce1259698588a3c3ed88f405db33b0", // just a random SHA-256 hash
	Location: Location{
		SiteArea: &SiteArea{
			URL:      "http://goat:8080/WebGoat/login",
			HTTPVerb: "GET",
			Evidence: "<form method=\"POST\" style=\"width: 200px;\" action=\"/WebGoat/login\">",
		},
	},
	Scanner: Scanner{
		ID:   "zap",
		Name: "Zed Attack Proxy",
	},
	Severity:   SeverityLevelLow,
	Confidence: ConfidenceLevelMedium,
	Mitigations: []Mitigation{
		{
			Phase:          "Architecture and Design",
			Recommendation: "Use a vetted library or framework that does not allow this weakness to occur or provides constructs that make this weakness easier to avoid.\nFor example, use anti-CSRF packages such as the OWASP CSRFGuard.",
		},
		{
			Phase:          "Implementation",
			Recommendation: "Ensure that your application is free of cross-site scripting issues, because most CSRF defenses can be bypassed using attacker-controlled script.",
		},
		{
			Phase:          "Architecture and Design",
			Recommendation: "Generate a unique nonce for each form, place the nonce into the form, and verify the nonce upon receipt of the form. Be sure that the nonce is not predictable (CWE-330).\nNote that this can be bypassed using XSS.\n\nIdentify especially dangerous operations. When the user performs a dangerous operation, send a separate confirmation request to ensure that the user intended to perform that operation.\nNote that this can be bypassed using XSS.\n\nUse the ESAPI Session Management control.\nThis control includes a component for CSRF.\n\nDo not use the GET method for any request that triggers a state change.",
		},
		{
			Phase:          "Implementation",
			Recommendation: "Check the HTTP Referer header to see if the request originated from an expected page. This could break legitimate functionality, because users or proxies may have disabled sending the Referer for privacy reasons.",
		},
	},
	Identifiers: []Identifier{
		CWEIdentifier(352),
		WASCIdentifier(9),
		ZapPluginIdentifier(10202),
	},
	Links: []Link{
		{
			URL: "http://cwe.mitre.org/data/definitions/352.html",
		},
		{
			URL: "http://projects.webappsec.org/Cross-Site-Request-Forgery",
		},
	},
}

var testDASTIssueJSON = `{
  "category": "dast",
  "name": "Absence of Anti-CSRF Tokens",
  "message": "No known Anti-CSRF token [anticsrf, CSRFToken, __RequestVerificationToken, csrfmiddlewaretoken, authenticity_token, OWASP_CSRFTOKEN, anoncsrf, csrf_token, _csrf, _csrfSecret] was found in the following HTML form: [Form 1: \"exampleInputEmail1\" \"exampleInputPassword1\" ].",
  "description": "No Anti-CSRF tokens were found in a HTML submission form\nA cross-site request forgery is an attack that involves forcing a victim to send an HTTP request to a target destination without their knowledge or intent in order to perform an action as the victim. The underlying cause is application functionality using predictable URL/form actions in a repeatable way. The nature of the attack is that CSRF exploits the trust that a web site has for a user. By contrast, cross-site scripting (XSS) exploits the trust that a user has for a web site. Like XSS, CSRF attacks are not necessarily cross-site, but they can be. Cross-site request forgery is also known as CSRF, XSRF, one-click attack, session riding, confused deputy, and sea surf.\n\nCSRF attacks are effective in a number of situations, including:\n    * The victim has an active session on the target site.\n    * The victim is authenticated via HTTP auth on the target site.\n    * The victim is on the same local network as the target site.\n\nCSRF has primarily been used to perform an action against a target site using the victim's privileges, but recent techniques have been discovered to disclose information by gaining access to the response. The risk of information disclosure is dramatically increased when the target site is vulnerable to XSS, because XSS can be used as a platform for CSRF, allowing the attack to operate within the bounds of the same-origin policy.\n",
  "cve": "32c8fea43be873a2f1186b801603ead805ce1259698588a3c3ed88f405db33b0",
  "severity": "Low",
  "confidence": "Medium",
  "mitigations": [
    {
      "phase": "Architecture and Design",
      "recommendation": "Use a vetted library or framework that does not allow this weakness to occur or provides constructs that make this weakness easier to avoid.\nFor example, use anti-CSRF packages such as the OWASP CSRFGuard."
    },
    {
      "phase": "Implementation",
      "recommendation": "Ensure that your application is free of cross-site scripting issues, because most CSRF defenses can be bypassed using attacker-controlled script."
    },
    {
      "phase": "Architecture and Design",
      "recommendation": "Generate a unique nonce for each form, place the nonce into the form, and verify the nonce upon receipt of the form. Be sure that the nonce is not predictable (CWE-330).\nNote that this can be bypassed using XSS.\n\nIdentify especially dangerous operations. When the user performs a dangerous operation, send a separate confirmation request to ensure that the user intended to perform that operation.\nNote that this can be bypassed using XSS.\n\nUse the ESAPI Session Management control.\nThis control includes a component for CSRF.\n\nDo not use the GET method for any request that triggers a state change."
    },
    {
      "phase": "Implementation",
      "recommendation": "Check the HTTP Referer header to see if the request originated from an expected page. This could break legitimate functionality, because users or proxies may have disabled sending the Referer for privacy reasons."
    }
  ],
  "scanner": {
    "id": "zap",
    "name": "Zed Attack Proxy"
  },
  "location": {
    "dependency": {
      "package": {}
    },
    "site_area": {
      "url": "http://goat:8080/WebGoat/login",
      "http_verb": "GET",
      "evidence": "\u003cform method=\"POST\" style=\"width: 200px;\" action=\"/WebGoat/login\"\u003e"
    }
  },
  "identifiers": [
    {
      "type": "cwe",
      "name": "CWE-352",
      "value": "352",
      "url": "https://cwe.mitre.org/data/definitions/352.html"
    },
    {
      "type": "wasc",
      "name": "WASC-09",
      "value": "9",
      "url": "http://projects.webappsec.org/Cross-Site-Request-Forgery"
    },
    {
      "type": "zap_plugin_id",
      "name": "ZAProxy plugin #10202",
      "value": "10202",
      "url": "https://github.com/zaproxy/zaproxy/blob/master/docs/scanners.md"
    }
  ],
  "links": [
    {
      "url": "http://cwe.mitre.org/data/definitions/352.html"
    },
    {
      "url": "http://projects.webappsec.org/Cross-Site-Request-Forgery"
    }
  ]
}`

var testContainerScanningIssue = Issue{
	Category:    CategoryContainerScanning,
	Message:     "CVE-2019-9511 in nghttp2",
	Description: "Some HTTP/2 implementations are vulnerable to window size manipulation and stream prioritization manipulation, potentially leading to a denial of service. The attacker requests a large amount of data from a specified resource over multiple streams. They manipulate window size and stream priority to force the server to queue the data in 1-byte chunks. Depending on how efficiently this data is queued, this can consume excess CPU, memory, or both.",
	CompareKey:  "debian:10:nghttp2:CVE-2019-9511",
	Severity:    SeverityLevelHigh,
	Solution:    "Upgrade nghttp2 from 1.36.0-2 to 1.36.0-2+deb10u1",
	Scanner: Scanner{
		ID:   "klar",
		Name: "klar",
	},
	Location: Location{
		Dependency: Dependency{
			Package: Package{
				Name: "nghttp2",
			},
			Version: "1.36.0-2",
		},
		OperatingSystem: "debian:10",
		Image:           "registry.gitlab.com/gitlab-org/security-products/dast/webgoat-8.0@sha256:bc09fe2e0721dfaeee79364115aeedf2174cce0947b9ae5fe7c33312ee019a4e",
	},
	Identifiers: []Identifier{
		{
			Type:  "cve",
			Name:  "CVE-2019-9511",
			Value: "CVE-2019-9511",
			URL:   "https://security-tracker.debian.org/tracker/CVE-2019-9511",
		},
	},
	Links: []Link{
		{
			Name: "",
			URL:  "https://security-tracker.debian.org/tracker/CVE-2019-9511",
		},
	},
}

const testContainerScanningIssueJSON = `{
  "category": "container_scanning",
  "message": "CVE-2019-9511 in nghttp2",
  "description": "Some HTTP/2 implementations are vulnerable to window size manipulation and stream prioritization manipulation, potentially leading to a denial of service. The attacker requests a large amount of data from a specified resource over multiple streams. They manipulate window size and stream priority to force the server to queue the data in 1-byte chunks. Depending on how efficiently this data is queued, this can consume excess CPU, memory, or both.",
  "cve": "debian:10:nghttp2:CVE-2019-9511",
  "severity": "High",
  "solution": "Upgrade nghttp2 from 1.36.0-2 to 1.36.0-2+deb10u1",
  "scanner": {
    "id": "klar",
    "name": "klar"
  },
  "location": {
    "dependency": {
      "package": {
        "name": "nghttp2"
      },
      "version": "1.36.0-2"
    },
    "operating_system": "debian:10",
    "image": "registry.gitlab.com/gitlab-org/security-products/dast/webgoat-8.0@sha256:bc09fe2e0721dfaeee79364115aeedf2174cce0947b9ae5fe7c33312ee019a4e"
  },
  "identifiers": [
    {
      "type": "cve",
      "name": "CVE-2019-9511",
      "value": "CVE-2019-9511",
      "url": "https://security-tracker.debian.org/tracker/CVE-2019-9511"
    }
  ],
  "links": [
    {
      "url": "https://security-tracker.debian.org/tracker/CVE-2019-9511"
    }
  ]
}`

const testEmptyIssueJSON = `{
  "category": "",
  "cve": "",
  "scanner": {
    "id": "",
    "name": ""
  },
  "location": {
    "dependency": {
      "package": {}
    }
  },
  "identifiers": null
}`

func TestIssue(t *testing.T) {

	t.Run("MarshalJSON", func(t *testing.T) {
		var tcs = []struct {
			Name          string
			Vulnerability Issue
			JSON          string
		}{
			{
				Name:          "SAST",
				Vulnerability: testSASTIssue,
				JSON:          testSASTIssueJSON,
			},
			{
				Name:          "DS",
				Vulnerability: testDependencyScanningVulnerability.ToIssue(),
				JSON:          testDependencyScanningVulnerabilityJSON,
			},
			{
				Name:          "DAST",
				Vulnerability: testDASTIssue,
				JSON:          testDASTIssueJSON,
			},
			{
				Name:          "Empty",
				Vulnerability: Issue{},
				JSON:          testEmptyIssueJSON,
			},
			{
				Name:          "CS",
				Vulnerability: testContainerScanningIssue,
				JSON:          testContainerScanningIssueJSON,
			},
		}

		for _, tc := range tcs {
			t.Run(tc.Name, func(t *testing.T) {
				b, err := json.Marshal(tc.Vulnerability)
				if err != nil {
					t.Fatal(err)
				}

				var buf bytes.Buffer
				json.Indent(&buf, b, "", "  ")
				got := buf.String()

				if got != tc.JSON {
					t.Errorf("Wrong result. Expected:\n%#v\nBut got:\n%#v", tc.JSON, got)
				}
			})
		}
	})

	t.Run("UnmarshalJSON", func(t *testing.T) {
		var tcs = []struct {
			Name  string
			JSON  string // JSON encoded vulnerability
			Issue Issue  // Issue to be JSON decoded
		}{
			{
				Name:  "SAST",
				JSON:  testSASTIssueJSON,
				Issue: testSASTIssue,
			},
			{
				Name:  "DS",
				JSON:  testDependencyScanningIssueJSON,
				Issue: testDependencyScanningVulnerability.Issue,
			},
			{
				Name:  "DAST",
				JSON:  testDASTIssueJSON,
				Issue: testDASTIssue,
			},
			{
				Name:  "ContainerScanning",
				JSON:  testContainerScanningIssueJSON,
				Issue: testContainerScanningIssue,
			},
			{
				Name:  "Empty",
				JSON:  testEmptyIssueJSON,
				Issue: Issue{},
			},
		}

		for _, tc := range tcs {
			t.Run(tc.Name, func(t *testing.T) {
				var got Issue
				if err := json.Unmarshal([]byte(tc.JSON), &got); err != nil {
					t.Fatal(err)
				}

				if !reflect.DeepEqual(got, tc.Issue) {
					t.Errorf("Wrong unmarshalled Issue. Expected:\n%#v\nBut got:\n%#v", tc.Issue, got)
				}
			})
		}
	})
}
