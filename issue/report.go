package issue

import "sort"

// Report is the output of an analyzer.
type Report struct {
	Version         Version          `json:"version"`
	TargetURL       string           `json:"target_url,omitempty"` // TargetURL is the URL of the website being scanned by DAST tool
	Vulnerabilities []Issue          `json:"vulnerabilities"`
	Remediations    []Remediation    `json:"remediations"`
	DependencyFiles []DependencyFile `json:"dependency_files,omitempty"`
	ScannedURLs     []URLScanResult  `json:"scanned_urls,omitempty"`  // ScannedURLs holds the URLs that were successfully scanned by DAST tool (the target server responded)
	IOErrorURLs     []URLScanResult  `json:"io_error_urls,omitempty"` // IOErrorURLs holds URLs scanned by DAST tool for which the target server did not respond
}

// Sort sorts vulnerabilities by decreasing severity.
func (r *Report) Sort() {
	// sort vulnerabilities by severity, compare key
	sort.Slice(r.Vulnerabilities, func(i, j int) bool {
		si, sj := r.Vulnerabilities[i].Severity, r.Vulnerabilities[j].Severity
		if si == sj {
			return r.Vulnerabilities[i].CompareKey < r.Vulnerabilities[j].CompareKey
		}
		return si > sj
	})

	// sort dependency files by path
	sort.Slice(r.DependencyFiles, func(i, j int) bool {
		return r.DependencyFiles[i].Path < r.DependencyFiles[j].Path
	})

	// sort dependencies by name, version
	for _, df := range r.DependencyFiles {
		sort.Slice(df.Dependencies, func(i, j int) bool {
			ni, nj := df.Dependencies[i].Package.Name, df.Dependencies[j].Package.Name
			if ni == nj {
				return df.Dependencies[i].Version < df.Dependencies[j].Version
			}
			return ni < nj
		})
	}
}

// ExcludePaths excludes paths from vulnerabilities, remediations, and dependency files.
// It takes a function that is true when the given path is excluded.
func (r *Report) ExcludePaths(isExcluded func(string) bool) {

	// filter vulnerabilities
	vulns := []Issue{}
	var rejCompareKeys []string
	for _, vuln := range r.Vulnerabilities {
		if isExcluded(vuln.Location.File) {
			rejCompareKeys = append(rejCompareKeys, vuln.CompareKey)
		} else {
			vulns = append(vulns, vuln)
		}
	}
	r.Vulnerabilities = vulns

	// filter remediations
	rems := []Remediation{}
remloop:
	for _, rem := range r.Remediations {
		for _, ref := range rem.Fixes {
			for _, ckey := range rejCompareKeys {
				if ckey == ref.CompareKey {
					continue remloop
				}
			}
		}
		rems = append(rems, rem)
	}
	r.Remediations = rems

	// filter dependencies
	depfiles := []DependencyFile{}
	for _, depfile := range r.DependencyFiles {
		if !isExcluded(depfile.Path) {
			depfiles = append(depfiles, depfile)
		}
	}
	r.DependencyFiles = depfiles
}

// Dedupe removes duplicates from vulnerabilities
func (r *Report) Dedupe() {
	r.Vulnerabilities = Dedupe(r.Vulnerabilities...)
}

// NewReport creates a new report in current version.
func NewReport() Report {
	return Report{
		Version:         CurrentVersion(),
		Vulnerabilities: []Issue{},
		Remediations:    []Remediation{},
		DependencyFiles: []DependencyFile{},
	}
}

// MergeReports merges the given reports and bring them to the current syntax version.
func MergeReports(reports ...Report) Report {
	report := NewReport()
	for _, r := range reports {
		report.Vulnerabilities = append(report.Vulnerabilities, r.Vulnerabilities...)
		report.Remediations = append(report.Remediations, r.Remediations...)
		report.DependencyFiles = append(report.DependencyFiles, r.DependencyFiles...)
	}
	report.Dedupe()
	report.Sort()
	return report
}
