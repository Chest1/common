package issue

import (
	"reflect"
	"testing"
)

func TestIdentifier(t *testing.T) {
	var tcs = []struct {
		Name  string
		Parse string // argument given to ParseIdentifier
		Got   Identifier
		Want  Identifier
	}{
		{
			Name:  "CVEIdentifier",
			Parse: "CVE-123",
			Got:   CVEIdentifier("CVE-123"),
			Want: Identifier{
				Type:  "cve",
				Name:  "CVE-123",
				Value: "CVE-123",
				URL:   "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-123",
			},
		},
		{
			Name:  "CWEIdentifier",
			Parse: "CWE-123",
			Got:   CWEIdentifier(123),
			Want: Identifier{
				Type:  "cwe",
				Name:  "CWE-123",
				Value: "123",
				URL:   "https://cwe.mitre.org/data/definitions/123.html",
			},
		},
		{
			Name:  "OSVDBIdentifier",
			Parse: "OSVDB-123",
			Got:   OSVDBIdentifier("OSVDB-123"),
			Want: Identifier{
				Type:  "osvdb",
				Name:  "OSVDB-123",
				Value: "OSVDB-123",
				URL:   "https://cve.mitre.org/data/refs/refmap/source-OSVDB.html",
			},
		},
		{
			Name:  "USNIdentifier",
			Parse: "USN-123",
			Got:   USNIdentifier("USN-123"),
			Want: Identifier{
				Type:  "usn",
				Name:  "USN-123",
				Value: "USN-123",
				URL:   "https://usn.ubuntu.com/123/",
			},
		},
		{
			Name:  "WASCIdentifier",
			Parse: "WASC-02",
			Got:   WASCIdentifier(2),
			Want: Identifier{
				Type:  "wasc",
				Name:  "WASC-02",
				Value: "2",
				URL:   "http://projects.webappsec.org/Insufficient-Authorization",
			},
		},
		// TODO: probably move to DAST wrapper project when it's created
		{
			Name:  "ZapPluginIdentifier",
			Parse: "ZAProxy plugin #10202",
			Got:   ZapPluginIdentifier(10202),
			Want: Identifier{
				Type:  "zap_plugin_id",
				Name:  "ZAProxy plugin #10202",
				Value: "10202",
				URL:   "https://github.com/zaproxy/zaproxy/blob/master/docs/scanners.md",
			},
		},
		{
			Name:  "RHSAIdentifier",
			Parse: "RHSA-2019:3892",
			Got:   RHSAIdentifier("RHSA-2019:3892"),
			Want: Identifier{
				Type:  "rhsa",
				Name:  "RHSA-2019:3892",
				Value: "RHSA-2019:3892",
				URL:   "https://access.redhat.com/errata/RHSA-2019:3892",
			},
		},
		{
			Name:  "ELSAIdentifier",
			Parse: "ELSA-2017-1101",
			Got:   ELSAIdentifier("ELSA-2017-1101"),
			Want: Identifier{
				Type:  "elsa",
				Name:  "ELSA-2017-1101",
				Value: "ELSA-2017-1101",
				URL:   "https://linux.oracle.com/errata/ELSA-2017-1101.html",
			},
		},
	}

	for _, tc := range tcs {
		t.Run(tc.Name, func(t *testing.T) {
			if !reflect.DeepEqual(tc.Got, tc.Want) {
				t.Errorf("Wrong result. Expected:\n%#v\nBut got:\n%#v", tc.Want, tc.Got)
			}
		})
	}

	t.Run("ParseIdentifier", func(t *testing.T) {
		for _, tc := range tcs {
			got, ok := ParseIdentifierID(tc.Parse)
			if !ok {
				t.Errorf("Expected %s to be parsed successfully", tc.Parse)
			}
			if !reflect.DeepEqual(got, tc.Want) {
				t.Errorf("Wrong result. Expected:\n%#v\nBut got:\n%#v", tc.Want, got)
			}
		}
	})
}
