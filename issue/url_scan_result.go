package issue

// URLScanResult represents the scanning result for a concrete URL on a target website scanned by URL scanning tool.
// Fields are mandatory unless explicitly annotated as optional.
type URLScanResult struct {
	URL                string `json:"url"`                            // URL stores the URL address that was scanned.
	Processed          bool   `json:"processed"`                      // Processed is true if there was a response from target server and no problems on spider side or false otherwise.
	ReasonNotProcessed string `json:"reason_not_processed,omitempty"` // ReasonNotProcessed is a message that can be a spider-related problem, a notification that this URL was filtered out, etc. Optional.
	HTTPVerb           string `json:"http_verb"`                      // HTTPVerb is "GET" or "POST" whichever was used to scan the URL.
	HTTPStatusCode     int    `json:"http_status_code,omitempty"`     // HTTPStatusCode is an integer XXX HTTP response code (e.g. 404); should be ignored if processed is false. Optional.
	HTTPStatusReason   string `json:"http_status_reason,omitempty"`   // HTTPStatusReason is a human-readable HTTP response header name, e.g. "Internal Server Error"; should be ignored if processed is false. Optional.
}
