package issue

import (
	"encoding/json"
	"strings"
)

// Issue represents a generic vulnerability occurrence reported by scanner.
type Issue struct {
	Category    Category        `json:"category"`              // Category describes where this vulnerability belongs (SAST, Dependency Scanning, etc...)
	Name        string          `json:"name,omitempty"`        // Name of the vulnerability, this must not include occurence's specific information.
	Message     string          `json:"message,omitempty"`     // Message is a short text that describes the vulnerability, it may include occurence's specific information.
	Description string          `json:"description,omitempty"` // Description is a long text that describes the vulnerability.
	CompareKey  string          `json:"cve"`                   // CompareKey is a value used to establish whether two issues are the same. Not reliable!
	Severity    SeverityLevel   `json:"severity,omitempty"`    // Severity describes how much the vulnerability impacts the software.
	Confidence  ConfidenceLevel `json:"confidence,omitempty"`  // Confidence describes how reliable the vulnerability's assessment is
	Solution    string          `json:"solution,omitempty"`    // Solution explains how to fix the vulnerability.
	Mitigations []Mitigation    `json:"mitigations,omitempty"` // Mitigations is the list of recommendations optionally labeled with phases
	Scanner     Scanner         `json:"scanner"`               // Scanner identifies the analyzer.
	Location    Location        `json:"location"`              // Location tells which class and/or method is affected by the vulnerability.
	Identifiers []Identifier    `json:"identifiers"`           // Identifiers are references that identify a vulnerability on internal or external DBs.
	Links       []Link          `json:"links,omitempty"`       // Links are external documentations or articles that further describes the vulnerability.
}

// Category is an identifier of the security scanning tool ("sast", "dast", etc.)
type Category string

const (
	// CategorySast is the identifier for "SAST" vulnerability category
	CategorySast = "sast"
	// CategoryDependencyScanning is the identifier for "Dependency Scanning" vulnerability category
	CategoryDependencyScanning = "dependency_scanning"
	// CategoryContainerScanning is the identifier for "Container Scanning" vulnerability category
	CategoryContainerScanning = "container_scanning"
	// CategoryDast is the identifier for "DAST" vulnerability category
	CategoryDast = "dast"
)

// SeverityLevel is the vulnerability severity level reported by scanner.
type SeverityLevel int

const (
	// SeverityLevelUndefined is a stub severity value for the case when it was not reported by scanner.
	SeverityLevelUndefined SeverityLevel = iota
	// SeverityLevelInfo represents the "info" or "ignore" severity level.
	SeverityLevelInfo
	// SeverityLevelUnknown represents the "experimental" or "unknown" severity level.
	SeverityLevelUnknown
	// SeverityLevelLow represents the "low" severity level.
	SeverityLevelLow
	// SeverityLevelMedium represents the "medium" severity level.
	SeverityLevelMedium
	// SeverityLevelHigh represents the "high" severity level.
	SeverityLevelHigh
	// SeverityLevelCritical represents the "critical" severity level.
	SeverityLevelCritical
)

// MarshalJSON converts a SeverityLevel value into the JSON representation
func (l SeverityLevel) MarshalJSON() ([]byte, error) {
	return json.Marshal(l.String())
}

// UnmarshalJSON parses a SeverityLevel value from JSON representation
func (l *SeverityLevel) UnmarshalJSON(b []byte) error {
	var s string
	if err := json.Unmarshal(b, &s); err != nil {
		return err
	}
	*l = ParseSeverityLevel(s)
	return nil
}

// ParseSeverityLevel parses a SeverityLevel value from string
func ParseSeverityLevel(s string) SeverityLevel {
	switch strings.ToLower(s) {
	case "critical":
		return SeverityLevelCritical
	case "high":
		return SeverityLevelHigh
	case "medium":
		return SeverityLevelMedium
	case "low":
		return SeverityLevelLow
	case "experimental", "unknown":
		return SeverityLevelUnknown
	case "ignore", "info":
		return SeverityLevelInfo
	default:
		return SeverityLevelUndefined
	}
}

func (l SeverityLevel) String() string {
	switch l {
	case SeverityLevelCritical:
		return "Critical"
	case SeverityLevelHigh:
		return "High"
	case SeverityLevelMedium:
		return "Medium"
	case SeverityLevelLow:
		return "Low"
	case SeverityLevelUnknown:
		return "Unknown"
	case SeverityLevelInfo:
		return "Info"
	}
	return ""
}

// ConfidenceLevel is the vulnerability confidence level reported by scanner.
type ConfidenceLevel int

const (
	// ConfidenceLevelUndefined is a stub confidence value for the case when it was not reported by scanner.
	ConfidenceLevelUndefined ConfidenceLevel = iota
	// ConfidenceLevelIgnore represents the "ignore" confidence level.
	ConfidenceLevelIgnore
	// ConfidenceLevelUnknown represents the "unknown" confidence level.
	ConfidenceLevelUnknown
	// ConfidenceLevelExperimental represents the "experimental" confidence level.
	ConfidenceLevelExperimental
	// ConfidenceLevelLow represents the "low" confidence level.
	ConfidenceLevelLow
	// ConfidenceLevelMedium represents the "medium" confidence level.
	ConfidenceLevelMedium
	// ConfidenceLevelHigh represents the "high" confidence level.
	ConfidenceLevelHigh
	// ConfidenceLevelConfirmed represents the "critical" or "confirmed" confidence level.
	ConfidenceLevelConfirmed
)

// MarshalJSON converts a ConfidenceLevel value into the JSON representation
func (l ConfidenceLevel) MarshalJSON() ([]byte, error) {
	return json.Marshal(l.String())
}

// UnmarshalJSON parses a ConfidenceLevel value from JSON representation
func (l *ConfidenceLevel) UnmarshalJSON(b []byte) error {
	var s string
	if err := json.Unmarshal(b, &s); err != nil {
		return err
	}
	*l = ParseConfidenceLevel(s)
	return nil
}

// ParseConfidenceLevel parses a ConfidenceLevel value from string
func ParseConfidenceLevel(s string) ConfidenceLevel {
	switch strings.ToLower(s) {
	case "critical", "confirmed":
		return ConfidenceLevelConfirmed
	case "high":
		return ConfidenceLevelHigh
	case "medium":
		return ConfidenceLevelMedium
	case "low":
		return ConfidenceLevelLow
	case "experimental":
		return ConfidenceLevelExperimental
	case "unknown":
		return ConfidenceLevelUnknown
	case "ignore":
		return ConfidenceLevelIgnore
	default:
		return ConfidenceLevelUndefined
	}
}

func (l ConfidenceLevel) String() string {
	switch l {
	case ConfidenceLevelConfirmed:
		return "Confirmed"
	case ConfidenceLevelHigh:
		return "High"
	case ConfidenceLevelMedium:
		return "Medium"
	case ConfidenceLevelLow:
		return "Low"
	case ConfidenceLevelExperimental:
		return "Experimental"
	case ConfidenceLevelUnknown:
		return "Unknown"
	case ConfidenceLevelIgnore:
		return "Ignore"
	}
	return ""
}

// Mitigation contains information about actions to execute in order to mitigate the vulnerability
type Mitigation struct {
	Phase          string `json:"phase,omitempty"` // Phase describes an SDLC phase where this mitigation should be applied (example: https://cwe.mitre.org/data/definitions/352.html#Potential_Mitigations)
	Recommendation string `json:"recommendation"`  // Recommendation describes action(s) to execute in order to mitigate the vulnerability (example: https://cwe.mitre.org/data/definitions/352.html#Potential_Mitigations)
}

// Location represents the location of the vulnerability occurrence
// be it a source code line, a dependency package identifier or
// whatever else.
type Location struct {
	File            string `json:"file,omitempty"`       // File is the path relative to the search path.
	LineStart       int    `json:"start_line,omitempty"` // LineStart is the first line of the affected code.
	LineEnd         int    `json:"end_line,omitempty"`   // LineEnd is the last line of the affected code.
	Class           string `json:"class,omitempty"`
	Method          string `json:"method,omitempty"`
	Dependency      `json:"dependency,omitempty"`
	SiteArea        *SiteArea `json:"site_area,omitempty"`        // SiteArea describes where the vulnerability was found on the scanned website
	OperatingSystem string    `json:"operating_system,omitempty"` // OperatingSystem is the operating system and optionally its version, separated by a semicolon: linux, debian:10, etc
	Image           string    `json:"image,omitempty"`            // Name of the Docker image
}

// Dependency contains the information about the software dependency
// (package details, version, etc.).
type Dependency struct {
	Package `json:"package,omitempty"`
	Version string `json:"version,omitempty"`
}

// Package contains the information about the software dependency package.
type Package struct {
	Name string `json:"name,omitempty"`
}

// SiteArea contains information about the vulnerable site area (page, form, HTTP header, etc.).
// All fields are mandatory unless stated otherwise (see fields' comments).
type SiteArea struct {
	URL       string `json:"url"`                  // URL where the vulnerability is located
	HTTPVerb  string `json:"http_verb"`            // HTTPVerb holds the HTTP verb used when the vulnerable URL was requested
	HTTPParam string `json:"http_param,omitempty"` // HTTPParam holds the HTTP param (GET or POST) used when the vulnerable URL was requested. Optional.
	Evidence  string `json:"evidence,omitempty"`   // Evidence is the exact vulnerability location: HTTP header, HTML form, cookie etc. Free form string, may contain anything including raw HTML. Optional.
}

// NewLinks generates new Link objects slice from the list of URLs.
func NewLinks(urls ...string) []Link {
	var links = make([]Link, len(urls))
	for i, url := range urls {
		links[i].URL = url
	}
	return links
}

// Link contains the hyperlink to the detailed information about a vulnerability.
type Link struct {
	Name string `json:"name,omitempty"` // Name of the link (optional)
	URL  string `json:"url"`            // URL of the document (mandatory)
}

// Scanner contains the identifying information about a security scanner.
type Scanner struct {
	ID   string `json:"id"`   // Id of the scanner as a snake_case string (mandatory)
	Name string `json:"name"` // Name of the scanner, for display purpose (mandatory)
}
