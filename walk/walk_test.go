package walk

import (
	"errors"
	"os"
	"path/filepath"
	"reflect"
	"testing"
)

// collectFunc returns a DirHandlerFunc that performs some check
// and append all the paths to the slice of strings that is returned.
// It returns returnErr if the visisted path matches errPath.
func collectFunc(t *testing.T, errPath string, returnErr error) (DirHandlerFunc, *[]string) {
	paths := []string{}
	return func(path string, info os.FileInfo, err error) error {
		if path == errPath {
			return returnErr
		}
		if err != nil {
			t.Errorf("Expecting no error in walk function but got %s for path %s", err, path)
			return errors.New("Unexpected error given to DirHandlerFunc")
		}
		if filepath.Base(path) != info.Name() {
			t.Errorf("FileInfo name %s doesn't match basename of path %s", info.Name(), path)
			return errors.New("Unexpected FileInfo given to DirHandlerFunc")
		}
		paths = append(paths, path)
		return nil
	}, &paths
}

func TestWalk(t *testing.T) {
	abort := errors.New("Abort")
	var tcs = []struct {
		Name      string
		Options   *Options
		ErrPath   string
		WalkErr   error
		WantPaths []string
		WantErr   error
	}{
		{
			Name: "with limited depth and ignored directories",
			Options: &Options{
				MaxDepth:         2,
				IgnoredDirs:      []string{"ignore-me", "ignore-me-too"},
				IgnoreHiddenDirs: true,
			},

			WantPaths: []string{"fixtures", "fixtures/first", "fixtures/second", "fixtures/a", "fixtures/b", "fixtures/c", "fixtures/c/third", "fixtures/d", "fixtures/a/a", "fixtures/b/b", "fixtures/b/b/fourth", "fixtures/c/c", "fixtures/c/c/fifth", "fixtures/d/see-me", "fixtures/d/see-me/seen"},
			WantErr:   nil,
		},
		{
			Name:      "with no limitations",
			WantPaths: []string{"fixtures", "fixtures/first", "fixtures/second", "fixtures/a", "fixtures/b", "fixtures/c", "fixtures/c/third", "fixtures/d", "fixtures/a/a", "fixtures/b/b", "fixtures/b/b/fourth", "fixtures/c/c", "fixtures/c/c/fifth", "fixtures/d/.hidden", "fixtures/d/.hidden/ignored", "fixtures/d/ignore-me", "fixtures/d/ignore-me/ignored", "fixtures/d/ignore-me-too", "fixtures/d/ignore-me-too/ignored", "fixtures/d/see-me", "fixtures/d/see-me/seen", "fixtures/a/a/a", "fixtures/a/a/a/deep"},
			WantErr:   nil,
		},
		{
			Name:      "when walk fails on a file",
			ErrPath:   "fixtures/second",
			WalkErr:   abort,
			WantPaths: []string{"fixtures", "fixtures/first"},
			WantErr:   abort,
		},
		{
			Name:      "when walk fails on a directory",
			ErrPath:   "fixtures/c",
			WalkErr:   abort,
			WantPaths: []string{"fixtures", "fixtures/first", "fixtures/second", "fixtures/a", "fixtures/b"},
			WantErr:   abort,
		},
	}

	for _, tc := range tcs {
		t.Run(tc.Name, func(t *testing.T) {
			wf, got := collectFunc(t, tc.ErrPath, tc.WalkErr)
			err := Walk("fixtures", wf, tc.Options)
			if err != tc.WantErr {
				t.Errorf("Wrong error. Expecting error to be %v but got %v", tc.WantErr, err)
			}
			if !reflect.DeepEqual(*got, tc.WantPaths) {
				t.Errorf("Wrong result. Expecting walk function to collect paths:\n%#v\nbut got:\n%#v", tc.WantPaths, *got)
			}
		})
	}
}
