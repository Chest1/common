package walk

import (
	"os"
	"path/filepath"
	"sort"
)

// Options makes it possible to ignore certain directories.
type Options struct {
	MaxDepth         int      // MaxDepth is the maximum depth of directory that are open.
	IgnoredDirs      []string // IgnoredDirs is a list of directory names (not paths) to be ignore.
	IgnoreHiddenDirs bool     // IngnoreHiddenDirs makes Walk ignore directory starting with ".".
}

// DirHandlerFunc is the type of the function called for each file or directory
// visited by Walk. The path argument contains the argument to Walk as a
// prefix; that is, if Walk is called with "dir", which is a directory
// containing the file "a", the walk function will be called with argument
// "dir/a". The info argument is the os.FileInfo for the named path.
//
// If there was a problem walking to the file or directory named by path, the
// incoming error will describe the problem and the function can decide how
// to handle that error (and Walk will not descend into that directory). If
// an error is returned, processing stops.
type DirHandlerFunc func(path string, info os.FileInfo, err error) error

// Entry contains a file path and a FileInfo struct.
type Entry struct {
	Path string
	Info os.FileInfo
}

// walk performs breadth-first search, calling walkFn.
func walk(entries []Entry, dirFn DirHandlerFunc, depth int, opts Options) error {
	dirs := []Entry{} // sub-directories
	for _, entry := range entries {
		path, info := entry.Path, entry.Info
		if !info.IsDir() {
			return dirFn(path, info, nil)
		}

		names, err := readDirNames(path)
		err1 := dirFn(path, info, err)
		if err != nil || err1 != nil {
			// The caller's behavior is controlled by the return value, which is decided
			// by dirFn. dirFn may ignore err and return nil.
			// So walk should return whatever dirFn returns.
			return err1
		}

		for _, name := range names {
			filename := filepath.Join(path, name)
			fileInfo, err := os.Lstat(filename)
			if err != nil {
				if err := dirFn(filename, fileInfo, err); err != nil {
					return err
				}
			} else if fileInfo.IsDir() {
				if !isIgnoredDirname(name, opts) {
					dirs = append(dirs, Entry{filename, fileInfo})
				}
			} else {
				if err := walk([]Entry{{filename, fileInfo}}, dirFn, depth, opts); err != nil {
					return err
				}
			}
		}
	}
	if len(dirs) == 0 {
		return nil
	}
	if opts.MaxDepth != -1 && depth > opts.MaxDepth {
		return nil
	}
	return walk(dirs, dirFn, depth+1, opts)
}

func isIgnoredDirname(dirname string, opts Options) bool {
	if opts.IgnoreHiddenDirs && dirname[0] == '.' {
		return true
	}
	for _, ignored := range opts.IgnoredDirs {
		if dirname == ignored {
			return true
		}
	}
	return false
}

// Walk walks the file tree rooted at root, calling walkFn for each file or
// directory in the tree, including root. All errors that arise visiting files
// and directories are filtered by walkFn. The files are walked in lexical
// order, which makes the output deterministic but means that for very
// large directories Walk can be inefficient.
// Walk does not follow symbolic links.
//
// It's similar to filepath.Walk but performs breadth-first search and support Options.
// Unlike filepath.Walk it doesn't process SkipDir errors.
func Walk(root string, walkFn DirHandlerFunc, opts *Options) error {
	if opts == nil {
		opts = &Options{MaxDepth: -1}
	}
	info, err := os.Lstat(root)
	if err != nil {
		err = walkFn(root, nil, err)
	} else {
		err = walk([]Entry{{root, info}}, walkFn, 1, *opts)
	}
	return err
}

// readDirNames reads the directory named by dirname and returns
// a sorted list of directory entries.
func readDirNames(dirname string) ([]string, error) {
	f, err := os.Open(dirname)
	if err != nil {
		return nil, err
	}
	names, err := f.Readdirnames(-1)
	f.Close()
	if err != nil {
		return nil, err
	}
	sort.Strings(names)
	return names, nil
}
