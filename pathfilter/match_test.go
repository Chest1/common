package pathfilter

import (
	"testing"
)

func TestMatch(t *testing.T) {
	var tcs = []struct {
		pattern string
		path    string
		matched bool
	}{
		// behaves like filepath.Match
		{"x", "x", true},
		{"x", "z", false},
		{"*", "x", true},
		{"x/y", "x/y", true},
		{"x/*", "x/y", true},
		{"x/y", "x/z", false},
		{"x/y/z", "x/y", false},
		{"a/b/c/d", "a/b", false},

		// file/dir name match
		{"x", "x/y", true},
		{"x", "y/x", true},
		{"x", "x/y/z", true},
		{"x", "x/y/z", true},
		{"*.xyz", "a/123.xyz", true},
		{"x/", "x", false},

		// match parent directory
		{"x/y", "x/y/z", true},
		{"a/b/c", "a/b/c/d", true},
		{"a/b/c", "a/b/c/d/e", true},
		{"a/b/*", "a/b/c/d/e", true},
		{"z/b/c", "a/b/c/d/e", false},

		// absolute pattern with absolute path
		{"/x", "/x/y", true},
		{"/x", "/y/x", false},
		{"/x", "/x/y/z", true},
		{"/x", "/x/y/z", true},
		{"/x/y", "/x/y/z", true},
		{"/a/b/c", "/a/b/c/d", true},
		{"/a/b/*", "/a/b/c/d", true},
		{"/a/b/c", "/a/b/c/d/e", true},
		{"/z/b/c", "/a/b/c/d/e", false},

		// absolute pattern with relative path
		{"/x", "x", true},
		{"/x", "x/y", true},
		{"/x", "y/x", false},
		{"/x", "x/y/z", true},
		{"/x", "x/y/z", true},
		{"/x/y", "x/y/z", true},
		{"/a/b/c", "a/b/c/d", true},
		{"/a/b/*", "a/b/c/d", true},
		{"/a/b/c", "a/b/c/d/e", true},
		{"/z/b/c", "a/b/c/d/e", false},
		{"/*.xyz", "a/123.xyz", false},
		{"a/*.xyz", "a/123.xyz", true},
	}

	for _, tc := range tcs {
		matched, err := Match(tc.pattern, tc.path)
		if err != nil {
			t.Error(err)
			continue
		}
		if tc.matched {
			if !matched {
				t.Errorf("Expected pattern '%s' to match path '%s'", tc.pattern, tc.path)
			}
		} else {
			if matched {
				t.Errorf("Expected pattern '%s' not to match path '%s'", tc.pattern, tc.path)
			}
		}
	}
}
