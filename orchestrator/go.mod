module gitlab.com/gitlab-org/security-products/analyzers/common/orchestrator/v2

require (
	github.com/docker/distribution v2.7.0-rc.0.0.20181002220433-1cb4180b1a5b+incompatible // indirect
	github.com/docker/docker v0.7.3-0.20181024032540-785fe99bdb7c // indirect
	github.com/opencontainers/go-digest v1.0.0-rc1 // indirect
	github.com/opencontainers/image-spec v1.0.1 // indirect
)
