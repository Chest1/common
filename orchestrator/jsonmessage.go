package orchestrator

import (
	"encoding/json"
	"fmt"
	"io"

	"github.com/docker/docker/pkg/jsonmessage"
)

// displayDotsWithJSONMessagesStream handles a json message stream from `in` to `out`, showing a dot
// per a json. See also https://godoc.org/github.com/docker/docker/pkg/jsonmessage#DisplayJSONMessagesStream
func displayDotsWithJSONMessagesStream(in io.Reader, out io.Writer) error {
	var (
		dec = json.NewDecoder(in)
	)

	for {
		var jm jsonmessage.JSONMessage
		if err := dec.Decode(&jm); err != nil {
			if err == io.EOF {
				fmt.Fprintf(out, "\n")
				break
			}
			return err
		}

		if (jm.Progress != nil && jm.Progress.Total != 0 && jm.Progress.Total != jm.Progress.Current) || jm.Aux != nil {
			continue
		}

		fmt.Fprintf(out, ".")
	}
	return nil
}
