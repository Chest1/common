module gitlab.com/gitlab-org/security-products/analyzers/common/table/v2

require (
	github.com/bbrks/wrap v2.3.0+incompatible // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/stretchr/testify v1.2.2 // indirect
)
