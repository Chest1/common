package command

import (
	"io"
	"os"

	"github.com/urfave/cli"
)

// AnalyzeFunc is a type for a function that runs the analyzer command against
// the files in project dir and emits the analyzer's output for further processing.
type AnalyzeFunc func(c *cli.Context, path string) (io.ReadCloser, error)

// Analyze returns a cli sub-command that wraps the analyzing the project and generating the report.
func Analyze(analyze AnalyzeFunc, analyzeFlags []cli.Flag) cli.Command {
	return cli.Command{
		Name:      "analyze",
		Aliases:   []string{"a"},
		Usage:     "Analyze detected project and generate report",
		ArgsUsage: "<project-dir>",
		Flags:     analyzeFlags,
		Action: func(c *cli.Context) error {
			// check args
			if len(c.Args()) != 1 {
				cli.ShowSubcommandHelp(c)
				return errInvalidArgs
			}

			// input file
			input := c.Args().First()
			f, err := analyze(c, input)
			if err != nil {
				return err
			}
			defer f.Close()

			if _, err := io.Copy(os.Stdout, f); err != nil {
				return err
			}
			return nil
		},
	}
}
